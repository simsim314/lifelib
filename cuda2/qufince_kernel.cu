#include "qufince.h"
#include "cudagol.h"
#include <chrono>

namespace apg {
__global__ void collision_kernel(const bpattern *things, uint32_t a_start, uint32_t a_end, uint32_t b_start, uint32_t b_end, int gens1, int gens2, uint32_t *sols) {

    uint32_t idx = ((threadIdx.x + blockIdx.x * blockDim.x) >> 5) + a_start;
    if (idx >= a_end) { return; }

    uint4 cx = load_bpattern(things);
    uint4 dx = load_bpattern(things + 1);

    uint4 ax = load_bpattern(things + idx);

    uint32_t bx_x = ax.x, bx_y = ax.y, bx_z = ax.z, bx_w = ax.w;

    for (uint32_t jdx = b_start; jdx < b_end; jdx++) {

        uint4 bx = load_bpattern(things + jdx);
        bx_x |= bx.x;
        bx_y |= bx.y;
        bx_z |= bx.z;
        bx_w |= bx.w;

        for (int i = 0; i < gens1; i++) {
            bx = advance_torus(bx);
        }

        uint32_t res = ((bx.x & cx.x) ^ dx.x) | ((bx.y & cx.y) ^ dx.y) | ((bx.z & cx.z) ^ dx.z) | ((bx.w & cx.w) ^ dx.w);

        if (hh::ballot_32(res != 0) == 0) {
            for (int i = 0; i < gens2; i++) {
                bx = advance_torus(bx);
            }
            res = ((bx.x & cx.x) ^ dx.x) | ((bx.y & cx.y) ^ dx.y) | ((bx.z & cx.z) ^ dx.z) | ((bx.w & cx.w) ^ dx.w);
        }

        if (hh::ballot_32(res != 0) == 0) {
            // solution found!
            if ((threadIdx.x & 31) == 0) {
                uint32_t k = hh::atomic_add(sols + 131072, 1) & 65535;
                sols[k*2] = idx;
                sols[k*2+1] = jdx;
            }
        }

        bx_x &= ~bx.x;
        bx_y &= ~bx.y;
        bx_z &= ~bx.z;
        bx_w &= ~bx.w;
    }
}

void run_collision_kernel(const bpattern& and_mask, const bpattern& target, const std::vector<bpattern> &a, const std::vector<bpattern> &b, int gens1, int gens2) {
bpattern* patterns_device;
bpattern* patterns_host;

size_t n_patterns = 2 + a.size() + b.size();

cudaMalloc(&patterns_device,   n_patterns * sizeof(bpattern));
cudaMallocHost(&patterns_host, n_patterns * sizeof(bpattern));

patterns_host[0] = and_mask;
patterns_host[1] = target;

size_t i = 2;

for (auto x : a) { patterns_host[i++] = x; }
for (auto x : b) { patterns_host[i++] = x; }

cudaMemcpy(patterns_device, patterns_host, n_patterns * sizeof(bpattern), cudaMemcpyHostToDevice);

uint32_t* sols_device;
uint32_t* sols_host;
cudaMalloc(&sols_device, 532480);
cudaMallocHost(&sols_host, 532480);
memset(sols_host, 0, 532480);
cudaMemcpy(sols_device, sols_host, 532480, cudaMemcpyHostToDevice);

uint32_t a_start = 2;
uint32_t a_end = a_start + a.size();

uint32_t chunksize = 128;
uint32_t blocksize = 256;

for (uint32_t bo = 0; bo < b.size(); bo += chunksize) {

    uint32_t b_start = a_end + bo;
    uint32_t b_end = a_end + hh::min(bo + chunksize, ((uint32_t) b.size()));

    uint32_t n_blocks = a.size() / (blocksize >> 5) + 1;
    uint64_t workload = gens1 * ((uint64_t) (a_end - a_start)) * ((uint64_t) (b_end - b_start));

    auto before = std::chrono::high_resolution_clock::now();
    collision_kernel<<<n_blocks, blocksize>>>(patterns_device, a_start, a_end, b_start, b_end, gens1, gens2, sols_device);
    cudaDeviceSynchronize();
    uint32_t sols_before = sols_host[131072];
    cudaMemcpy(sols_host, sols_device, 532480, cudaMemcpyDeviceToHost);
    uint32_t n_sols = sols_host[131072] - sols_before;
    auto after = std::chrono::high_resolution_clock::now();

    uint64_t microseconds = std::chrono::duration_cast<std::chrono::microseconds>(after - before).count();

    std::cerr << "# progress: " << (b_end - a_end) << "/" << b.size() << "; speed = " << (workload / microseconds) << "M iters/sec" << std::endl; 

    if (n_sols > 0) {
        std::cerr << "#" << n_sols << " solutions." << std::endl;
        if (n_sols > 65536) { n_sols = 65536; }
        for (uint32_t i = 0; i < n_sols; i++) {
            uint32_t so = (i + sols_before) & 65535;
            uint32_t idx = sols_host[so*2];
            uint32_t jdx = sols_host[so*2+1];

              for (int k = 0; k < 64; k++) {
                    uint64_t row = patterns_host[idx].x[k] | patterns_host[jdx].x[k];
                    char s[66] = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n";
                    for (int l = 0; l < 64; l++) {
                        s[l] = ((row >> l) & 1) ? 'o' : '.';
                    }
                    std::cout << s;
                }
                std::cout << std::endl;
            }
        }
    }

    cudaFreeHost(sols_host);
    cudaFree(sols_device);
    cudaFreeHost(patterns_host);
    cudaFree(patterns_device);


}
}

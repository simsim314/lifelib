#!/bin/bash
g++ -O3 -c cuda2/qufince_main.cpp -o qufince_main.o
nvcc -O3 -c cuda2/qufince_kernel.cu -o qufince_kernel.o
nvcc qufince_main.o qufince_kernel.o -o qufince
